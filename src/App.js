import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { fetchArticleStart } from './redux/actions/appAction';
import Navigation from './components/layout/Navigation/Navigation';
import './App.css';

const App = ({ fetchArticle, dispatch, articles, isLoading, sortedArticles }) => {
  useEffect(() => dispatch(fetchArticle), [dispatch, fetchArticle])

  return (
    <div className="app">
      <Navigation />
    </div>
  );
}
const mapStateToProps = ({ appReducer, inputReducer }) => {
  const { sortedArticles } = inputReducer
  const { articles, isLoading } = appReducer;
  return ({
    articles,
    isLoading,
    sortedArticles

  })

}


const mapDispatchToProps = dispatch => ({
  fetchArticle: () => (dispatch(fetchArticleStart())),
  dispatch
})
export default connect(mapStateToProps, mapDispatchToProps)(App);
