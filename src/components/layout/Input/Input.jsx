import React, { useState, useRef } from 'react'
import { connect } from 'react-redux'
import matchSorter from 'match-sorter'
import { makeStyles } from '@material-ui/core/styles'
import {
  putArticleFromInputStart,
  putNewArticle
} from '../../../redux/actions/inputAction'
import {
  getDataFromState,
  getSortedArticlesState,
  getArticlesState
} from '../../../redux/selectors/inputSelectors'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import Divider from '@material-ui/core/Divider'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import SearchIcon from '@material-ui/icons/Search'
import DirectionsIcon from '@material-ui/icons/Directions'
import profil from '../assets/Oval 2.png'

const useStyles = makeStyles(theme => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '610px',
    height: '98px',
    borderRadius: '0',
    boxShadow: '0 0.2px .2px 0'
  },
  input: {
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(1),
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 29,
    margin: 4
  }
}))

const CustomizedInputBase = ({
  dispatch,
  putArticle,
  query,
  articles,
  isLoading,
  saveArrayWithArticles,
  sortedArticles
}) => {
  // const { query, sortedArticles, articles } = props
  const [value, changeValue] = useState({
    find: 'Find a user, team, meeting',
    id: 'search'
  })
  const inputLabel = useRef(null)
  const handleChange = event => {
    changeValue(event.target.value)
    dispatch({ type: 'INPUT_CHOOSE_ARTICLE_SAGA', value })
  }
  const sortedArray = (array, search) => {
    const arrayWithArticles = matchSorter(array, search, {
      keys: ['tags', 'author']
    })
    console.log(arrayWithArticles)
    dispatch(saveArrayWithArticles(arrayWithArticles))
  }
  // if (query.length > 0) {
  sortedArray(articles, query)
  // }

  const classes = useStyles()

  return (
    <Paper className={classes.root}>
      <SearchIcon style={{ opacity: '.5', marginLeft: '20px' }} />
      <InputBase
        border={1}
        className={classes.input}
        placeholder={value.find}
        inputProps={{ 'aria-label': 'search google maps' }}
        onChange={handleChange}
      />
      <IconButton className={classes.iconButton} aria-label='search' />
      <Divider className={classes.divider} orientation='vertical' />
      <IconButton
        color='primary'
        className={classes.iconButton}
        aria-label='directions'>
        <img src={profil} alt='profil' style={{ width: '58px' }} />
      </IconButton>
    </Paper>
  )
}
const MapStateToProps = state => ({
  query: getDataFromState(state),
  sortedArticles: getSortedArticlesState(state),
  articles: getArticlesState(state)
})

const MapDispatchToProps = dispatch => {
  return {
    putArticle: () => dispatch(putArticleFromInputStart()),
    saveArrayWithArticles: (...articles) => dispatch(putNewArticle(articles)),
    dispatch
  }
}
export default connect(
  MapStateToProps,
  MapDispatchToProps
)(CustomizedInputBase)
