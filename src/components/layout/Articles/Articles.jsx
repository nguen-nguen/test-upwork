import React from 'react'
import { connect } from 'react-redux'
import store from '../../../redux/store/index'

const ListArticles = ({ articles, isLoading, sortedArticles }) => {
  return (
    <ul>
      {articles &&
        articles.map((item, index) => (
          <span key={index}>
            <p>{item.author}</p>
            <p>
              <img src={item.avatar} alt='logo' />
            </p>
            <p>{item.title}</p>
            <p>{item.email}</p>
            <p>{item.jobTitle}</p>
            <p>{item.desc}</p>
            <p>{item.id}</p>
            <p>{item.tags}</p>
          </span>
        ))}
    </ul>
  )
}
const MapStateToProps = ({ appReducer, inputReducer }) => {
  const { articles, isLoading } = appReducer
  const { sortedArticles } = inputReducer
  console.log(sortedArticles)
  return {
    articles,
    isLoading,
    sortedArticles
  }
}
const MapDispatchToProps = ({ dispatch }) => {
  return {
    dispatch
  }
}
export default connect(
  MapStateToProps,
  MapDispatchToProps
)(ListArticles)
