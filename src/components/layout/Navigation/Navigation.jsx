import React from 'react'
import CustomizedInputBase from '../Input/Input'
import ListArticles from '../Articles/Articles'
import logo from '../assets/logo.png'
import setting from '../assets/settings icon.svg'
import chat from '../assets/sessions icon.svg'
import fill from '../assets/files board icon.svg'
import icon from '../assets/dashboard icon.svg'
import meeting from '../assets/meetings icon.svg'
import session from '../assets/sessions icon.svg'
import conversation from '../assets/conversations icon.svg'
import { grey } from '@material-ui/core/colors'
const Navigation = () => {
  const url = '`http://fillmurray.com/50/50'
  const styles = {
    width: '100px',
    borderRadius: '1px lightgray'
  }
  return (
    <>
      <div className='row'>
        <div className='col col-panel'>
          <div className='logo'>
            <img className='logo__img' src={logo} alt='' />
          </div>
          <nav className='nav'>
            <div className='nav__item active'>
              <img src={icon} alt='icon active' />
            </div>
            <div className='nav__item'>
              <img src={chat} alt='chat' />
            </div>
            <div className='nav__item'>
              <img src={meeting} alt='meeting' />
            </div>
            <div className='nav__item'>
              <img src={fill} alt='fill' />
            </div>
            <div className='nav__item'>
              <img src={conversation} alt='conversation' />
            </div>
            <div className='nav__item'>
              <img src={setting} alt='setting' />
            </div>
          </nav>
        </div>
        <div className='col col-base navigation'>
          <div className='search'>
            <CustomizedInputBase />
          </div>
          <main className='main'>
            <ListArticles />
          </main>
        </div>
      </div>
    </>
  )
}

export default Navigation
