import axios from 'axios';
import store from '../../redux/store/index'
//import store from '../../store/index';

console.log(store)
class Api {
  constructor(endpoint) {
    this.endpoint = endpoint;
  }
  //fetch articles from json-server
  fetchArticlesFromJsonServer = async (endpoint) => {
    return await axios.get(
      'http://localhost:3000/articles',
    );
  }
  //fetch sorted articles
  fetchSortedArticles = async (endpoint, query) => {
    return await axios.get(`${endpoint}?author=${query}`);
  }
};


export default Api;
