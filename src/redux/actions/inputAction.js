import actionCreator from '../../utils/makeActionCreator'
export const INPUT_CHOOSE_ARTICLE_SAGA = 'INPUT_CHOOSE_ARTICLE_SAGA';
export const putArticleFromInputStart = actionCreator(INPUT_CHOOSE_ARTICLE_SAGA, "payload");

export const INPUT_PUT_ARTICLE_SAGA = 'INPUT_PUT_ARTICLE_SAGA ';
export const putNewArticle = actionCreator(INPUT_PUT_ARTICLE_SAGA, "payload");

export const INPUT_PUT_ARTICLE_SAGA_ERROR = 'INPUT_PUT_ARTICLE_SAGA_ERROR';
export const putArticleFromInputError = actionCreator(INPUT_PUT_ARTICLE_SAGA_ERROR, "payload");
