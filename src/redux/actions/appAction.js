import actionCreator from '../../utils/makeActionCreator'

// fetch articles  from json-server
export const APP_FETCH_DATA_SAGA_START = 'APP_FETCH_DATA_SAGA_START';
export const fetchArticleStart = actionCreator(APP_FETCH_DATA_SAGA_START, "payload");

export const APP_FETCH_DATA_SAGA_SUCCESS = 'APP_FETCH_DATA_SAGA_SUCCESS';
export const fetchArticlesSuccess = actionCreator(APP_FETCH_DATA_SAGA_SUCCESS, "payload");

export const APP_FETCH_DATA_SAGA_ERROR = 'APP_FETCH_DATA_SAGA_ERROR';
export const fetchArticlesError = actionCreator(APP_FETCH_DATA_SAGA_ERROR, "payload");
