import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { rootSaga } from '../sagas/rootSaga'
import rootReducer from '../reducers/rootReducer'
const configureStore = () => {
  // Note: passing middleware as the last argument to createStore requires redux@>=3.1.0
  const sagaMiddleware = createSagaMiddleware();
  const middleWares = [sagaMiddleware, thunk, logger];
  const enhancer = compose(...[applyMiddleware(...middleWares)]);
  const store = createStore(rootReducer, {}, enhancer)
  sagaMiddleware.run(rootSaga)
  return store;
}
export default configureStore();
