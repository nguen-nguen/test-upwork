import { combineReducers } from 'redux';
import { appReducer } from './appReducer'
import { inputReducer } from './inputReducer'


const rootReducer = combineReducers({
  appReducer,
  inputReducer
});
export default rootReducer;
