import { createSelector } from 'reselect'
import {
  INPUT_CHOOSE_ARTICLE_SAGA,
  INPUT_PUT_ARTICLE_SAGA,
  INPUT_PUT_ARTICLE_SAGA_ERROR

} from '../actions/inputAction'

const initialState = Object.freeze({
  query: '',
  sortedArticles: [],
  isPutting: false
})

export const inputReducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_CHOOSE_ARTICLE_SAGA:
      return {
        ...state,
        query: action.value,
      };
    case INPUT_PUT_ARTICLE_SAGA:
      return {
        ...state,
        sortedArticles: action.arrayWithArticles,
      };
    case INPUT_PUT_ARTICLE_SAGA_ERROR:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
}