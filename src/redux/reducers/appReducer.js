import {
  APP_FETCH_DATA_SAGA_START,
  APP_FETCH_DATA_SAGA_SUCCESS,
  APP_FETCH_DATA_SAGA_ERROR
} from '../actions/appAction'

const initialState = Object.freeze({
  articles: [],
  isLoading: false,
})

export const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case APP_FETCH_DATA_SAGA_START:
      return {
        ...state,
        isLoading: true,
      };
    case APP_FETCH_DATA_SAGA_SUCCESS:
      return {
        ...state,
        isLoading: false,
        articles: action.payload.data,
      };
    case APP_FETCH_DATA_SAGA_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };

    default:
      return state;
  }


}