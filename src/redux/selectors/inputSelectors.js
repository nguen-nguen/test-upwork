import { createSelector } from 'reselect'
const getQuery = state => state.inputReducer.query;
const getSortedArticles = state => state.inputReducer.sortedArticles;
const getArticles = state => state.appReducer.articles;

// reselect function
export const getDataFromState = createSelector(
  [getQuery],
  (query) => query,
)
export const getSortedArticlesState = createSelector(
  [getSortedArticles],
  (sortedArticles) => sortedArticles,
)
export const getArticlesState = createSelector(
  [getArticles],
  (articles) => articles,
)
