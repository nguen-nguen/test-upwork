import { put, takeEvery, all } from 'redux-saga/effects'
import { appSaga } from './appSaga';
import { inputSaga } from './inputSaga'
export function* rootSaga() {
  console.log('Hello Sagas!')
  yield all([
    appSaga(),
    inputSaga(),
  ])
}