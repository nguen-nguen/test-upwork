import { call, put, delay, select } from 'redux-saga/effects';
import Api from '../../components/API/index';
import { putNewArticle, putArticleFromInputSuccess, putArticleFromInputError } from '../actions/inputAction'
import store from '../store/index'
console.log(store)

export function* inputSaga() {
  const api = new Api();
  try {
    const endpoint = process.env.REACT_APP_ENDPOINT;
    const name = yield (select(state => state.queue))
    console.log(name)
    const article = yield call(api.fetchSortedArticles, endpoint);
    console.log(article)
    yield delay(2000)
    //yield put(putNewArticle(article))
  } catch (e) {
    yield put(putArticleFromInputError(e))
  }

}