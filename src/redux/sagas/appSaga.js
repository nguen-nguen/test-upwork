import { call, put, delay } from 'redux-saga/effects';
import Api from '../../components/API/index';
import { fetchArticlesSuccess, fetchArticlesError } from '../actions/appAction'


export function* appSaga() {
  const api = new Api();
  try {
    const articles = yield call(api.fetchArticlesFromJsonServer, process.env.REACT_APP_ENDPOINT)
    yield delay(1000)
    yield put(fetchArticlesSuccess(articles))
  } catch (error) {
    yield put(fetchArticlesError(error))
  }
}